<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Plasare Comandă</title>
    <link rel="stylesheet" href="com.css">
</head>
<body>
    <div class="container">
        <h1>Plasare Comandă</h1>
        <div class="cart-items">
            <?php
            session_start();
            // Verificăm dacă există produse în coș
            if (!empty($_SESSION['cart'])) {
                // Afisăm produsele din coș
                foreach ($_SESSION['cart'] as $product) {
                    ?>
                    <div class="cart-item">
                        <img src="<?php echo $product['imagine']; ?>" alt="<?php echo $product['nume']; ?>">
                        <div class="details">
                            <h3><?php echo $product['nume']; ?></h3>
                            <p>Preț: $<?php echo $product['pret']; ?></p>
                        </div>
                    </div>
                    <?php
                }
            } else {
                echo "<p>Coșul este gol.</p>";
            }
            ?>
        </div>
        <hr>
        <h2>Informații de livrare</h2>
        <form action="procesare_comanda.php" method="post">
            <div class="form-group">
                <label for="nume">Nume:</label>
                <input type="text" id="nume" name="nume" required>
            </div>
            <div class="form-group">
                <label for="adresa">Adresă:</label>
                <input type="text" id="adresa" name="adresa" required>
            </div>
            <div class="form-group">
                <label for="telefon">Telefon:</label>
                <input type="text" id="telefon" name="telefon" required>
            </div>
            <div class="form-group">
                <label for="plata">Metoda de plată:</label>
                <select id="plata" name="plata" required onchange="showCardDetails()">
                    <option value="la_livrare">La livrare</option>
                    <option value="card_bancar">Card bancar</option>
                </select>
            </div>
            <div id="cardDetails" style="display: none;">
                <h3>Detalii card bancar</h3>
                <div class="form-group">
                    <label for="numar_card">Număr card:</label>
                    <input type="text" id="numar_card" name="numar_card">
                </div>
                <div class="form-group">
                    <label for="expirare">Data expirării:</label>
                    <input type="text" id="expirare" name="expirare">
                </div>
                <div class="form-group">
                    <label for="cvv">CVV:</label>
                    <input type="text" id="cvv" name="cvv">
                </div>
            </div>
            <button type="submit">Plasează Comanda</button>
        </form>
    </div>

    <script>
        function showCardDetails() {
            var selectBox = document.getElementById("plata");
            var cardDetails = document.getElementById("cardDetails");
            if (selectBox.value === "card_bancar") {
                cardDetails.style.display = "block";
            } else {
                cardDetails.style.display = "none";
            }
        }
    </script>
</body>
</html>
