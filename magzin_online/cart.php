<?php
session_start();
if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])) {
    require_once("CreateDb.php");
    require_once("component.php");

    $db = new CreateDb("retro_fashion", "produse");

    if (isset($_GET['action']) && $_GET['action'] == 'remove') {
        if (isset($_GET['id'])) { 
            $remove_id = $_GET['id'];
            foreach ($_SESSION['cart'] as $key => $value) {
                if ($value["id"] == $remove_id) { 
                    unset($_SESSION['cart'][$key]);
                    echo "<script>alert('Produsul a fost eliminat din coș!')</script>";
                    echo "<script>window.location = 'cart.php'</script>";
                }
            }
        }
    }

    
}
?>
<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cart</title>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="style.css">
</head>
<body class="bg-light">
    <?php require_once('header.php'); ?>
    <div class="container-fluid">
        <div class="row px-5">
            <div class="col-md-7">
                <div class="shopping-cart">
                    <h6>Coșul Meu</h6>
                    <hr>
                    <?php
                    $total = 0;
                    if (isset($_SESSION['cart'])) {
                        foreach ($_SESSION['cart'] as $product) {
                            $result = $db->getDataById($product['id']);
                            $row = mysqli_fetch_assoc($result);
                            cartElement($row['imagine'], $row['nume'], $row['pret'], $row['id']);
                            $total += (int)$row['pret'];
                        }
                    } else {
                        echo "<h5>Coșul este gol</h5>";
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-4 offset-md-1 border rounded mt-5 bg-white">
                <div class="pt-4">
                    <h6>DETALII PREȚ</h6>
                    <hr>
                    <div class="row price-details">
                        <div class="col-md-6">
                            <?php
                            if (isset($_SESSION['cart'])) {
                                $count  = count($_SESSION['cart']);
                                echo "<h6>Preț ($count produse)</h6>";
                            } else {
                                echo "<h6>Preț (0 produse)</h6>";
                            }
                            ?>
                            <h6>Transport gratuit</h6>
                            <hr>
                            <h6>Total de plată</h6>
                        </div>
                        <div class="col-md-6">
                            <h6>$<?php echo $total; ?></h6>
                            <h6 class="text-success">GRATUIT</h6>
                            <hr>
                            <h6>$<?php echo $total; ?></h6>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary btn-block mt-3" onclick="window.location.href = 'com.php'">Plasează Comanda</button>

                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
