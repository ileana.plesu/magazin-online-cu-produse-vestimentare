<?php
class CreateDb
{
    public $servername;
    public $username;
    public $password;
    public $dbname;
    public $tablename;
    public $con;

    // Constructorul clasei
    public function __construct(
        $dbname = "retro_fashion",
        $tablename = "produse",
        $servername = "localhost",
        $username = "root",
        $password = ""
    ) {
        $this->dbname = $dbname;
        $this->tablename = $tablename;
        $this->servername = $servername;
        $this->username = $username;
        $this->password = $password;

        // Creăm conexiunea la baza de date
        $this->con = mysqli_connect($servername, $username, $password, $dbname);

        // Verificăm conexiunea
        if (!$this->con) {
            die("Conexiunea la baza de date a eșuat: " . mysqli_connect_error());
        }
    }

    // Metodă pentru a obține datele unui produs după ID
    public function getDataById($id)
    {
        $sql = "SELECT * FROM $this->tablename WHERE id=$id";
        $result = mysqli_query($this->con, $sql);
        return $result;
    }

    // Metodă pentru a obține toate datele din tabel
    public function getData()
    {
        $sql = "SELECT * FROM $this->tablename";
        $result = mysqli_query($this->con, $sql);
        return $result;
    }

    
    
        // Metodă pentru actualizarea stocului unui produs
        public function updateStock($productId, $newStock) {
            $sql = "UPDATE produse SET stoc = $newStock WHERE id = $productId";
            $result = $this->con->query($sql);
            if (!$result) {
                echo "Eroare la actualizarea stocului: " . $this->con->error;
            }
        }
    }
    


?>
