<?php
session_start(); // Inițializează sau preia sesiunea existentă



$mysqli = new mysqli("localhost", "root", "", "retro_fashion");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $mysqli->real_escape_string($_POST['email']);
    $password = $mysqli->real_escape_string($_POST['password']);

    $sql = "SELECT * FROM users WHERE email = '$email'";
    $result = $mysqli->query($sql);

    if ($result->num_rows == 1) {
        $user = $result->fetch_assoc();
        if (password_verify($password, $user['password'])) {
            $_SESSION['id'] = $user['id'];
            $_SESSION['nume_client'] = $user['nume'];
            header("Location: signup.php");
            exit();
        } else {
            echo "Email sau parolă incorectă!";
        }
    } else {
        echo "Email sau parolă incorectă!";
    }
}
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comenzile tale</title>
    <link rel="stylesheet" href="comenzi.css"> <!-- Stilizare CSS pentru pagina de comenzii -->
</head>
<body>
    <div class="container">
        <h1>Comenzile tale</h1>
        <div class="comenzi-list">
            <?php
            // Conectare la baza de date
            $mysqli = new mysqli("localhost", "root", "", "retro_fashion");

            // Verifică conexiunea
            if ($mysqli->connect_error) {
                die("Conexiunea la baza de date a eșuat: " . $mysqli->connect_error);
            }

            // Extrage comenzile utilizatorului din baza de date
            if(isset($_SESSION['nume_client'])) {
                $numeClient = $mysqli->real_escape_string($_SESSION['nume_client']);
                $sql = "SELECT * FROM comenzi WHERE nume_client = '$numeClient'";
                $result = $mysqli->query($sql);

                // Verifică dacă există comenzile
                if ($result) {
                    if ($result->num_rows > 0) {
                      
                        while($row = $result->fetch_assoc()) {
                            echo "<div class='comanda'>";
                            echo "<h3>Comanda #" . $row["id"] . "</h3>";
                            echo "<p>Nume client: " . $row["nume_client"] . "</p>";
                            echo "<p>Adresa: " . $row["adresa"] . "</p>";
                            echo "<p>Telefon: " . $row["telefon"] . "</p>";
                            echo "<p>Metoda de plată: " . $row["metoda_plata"] . "</p>";
                            echo "<p>Data comandă: " . $row["data_comanda"] . "</p>";
                            
                        }
                    } else {
                        echo "<p>Nu ai nicio comandă.</p>";
                    }
                } else {
                    echo "<p>Eroare în interogare: " . $mysqli->error . "</p>";
                }
            } else {
                echo "<p>ID-ul utilizatorului nu este setat în sesiune.</p>";
            }

            // Închide conexiunea la baza de date
            $mysqli->close();
            ?>
        </div>
    </div>
</body>
</html>