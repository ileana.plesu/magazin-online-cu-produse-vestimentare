<?php
session_start();

require_once ('CreateDb.php');
require_once('component.php');

// Creăm o instanță a clasei Createdb
$database = new CreateDb("retro_fashion", "produse");



// Verificăm dacă s-a trimis o cerere de adăugare a unui produs în coș
if (isset($_POST['add_to_cart'])) {
    $productId = $_POST['id'];

    
    $result = $database->getDataById($productId);
    $row = mysqli_fetch_assoc($result);
    $stock = $row['stoc'];

    
    if ($stock > 0) {
      
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = [];
        }

        // Verificăm dacă produsul este deja în coș
        $item_array_id = array_column($_SESSION['cart'], "id");
        if (in_array($productId, $item_array_id)) {
            echo "<script>alert('Produsul este deja adăugat în coș..!')</script>";
            echo "<script>window.location = 'index.php'</script>";
        } else {
            // Creăm un array asociativ cu detalii despre produs
            $productDetails = [
                'id' => $row['id'],
                'nume' => $row['nume'],
                'pret' => $row['pret'],
                'imagine' => $row['imagine']
            ];

          
            $_SESSION['cart'][] = $productDetails;

           
            $newStock = $stock - 1;
            $database->updateStock($productId, $newStock);
        }
    } else {
        echo "<script>alert('Produsul nu este disponibil în stoc..!')</script>";
        echo "<script>window.location = 'index.php'</script>";
    }
}

?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Retro Future - Produse</title>
    <link rel="stylesheet" href="haine.css">
    
</head>

<body>

    <div class="header">
        <h1>Retro Fashion</h1>
        <p>Descoperă cele mai noi și cool produse retro!</p>
    </div>
    <div class="button-container">
        <!-- Butonul de acasă -->
        <button class="button">
          <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 1024 1024" stroke-width="0" fill="currentColor" stroke="currentColor" class="icon">
            <path d="M946.5 505L560.1 118.8l-25.9-25.9a31.5 31.5 0 0 0-44.4 0L77.5 505a63.9 63.9 0 0 0-18.8 46c.4 35.2 29.7 63.3 64.9 63.3h42.5V940h691.8V614.3h43.4c17.1 0 33.2-6.7 45.3-18.8a63.6 63.6 0 0 0 18.7-45.3c0-17-6.7-33.1-18.8-45.2zM568 868H456V664h112v204zm217.9-325.7V868H632V640c0-22.1-17.9-40-40-40H432c-22.1 0-40 17.9-40 40v228H238.1V542.3h-96l370-369.7 23.1 23.1L882 542.3h-96.1z"></path>
          </svg>
          Acasă
        </button>
        <!-- Butonul de căutare -->
        <button class="button">
          <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" aria-hidden="true" viewBox="0 0 24 24" stroke-width="2" fill="none" stroke="currentColor" class="icon">
            <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" stroke-linejoin="round" stroke-linecap="round"></path>
          </svg>
          Căutare
        </button>
        <!-- Butonul de cont -->
        <button class="button">
          <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" stroke-width="0" fill="currentColor" stroke="currentColor" class="icon">
            <path d="M12 2.5a5.5 5.5 0 0 1 3.096 10.047 9.005 9.005 0 0 1 5.9 8.181.75.75 0 1 1-1.499.044 7.5 7.5 0 0 0-14.993 0 .75.75 0 0 1-1.5-.045 9.005 9.005 0 0 1 5.9-8.18A5.5 5.5 0 0 1 12 2.5ZM8 8a4 4 0 1 0 8 0 4 4 0 0 0-8 0Z"></path>
          </svg>
          Cont
        </button>
        <!-- Butonul de coș -->
        <button class="button" id="cartButton" onclick="window.location.href = 'cart.php';">
            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" stroke-linejoin="round" stroke-linecap="round" viewBox="0 0 24 24" stroke-width="2" fill="none" stroke="currentColor" class="icon">
                <circle r="1" cy="21" cx="9"></circle>
                <circle r="1" cy="21" cx="20"></circle>
                <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
            </svg>
            Coș
        </button>
    </div>
    <div id="message-container"></div>

    <div class="container">
        <div class="row text-center py-5">
            <?php
             
                $result = $database->getData();
                while ($row = mysqli_fetch_assoc($result)){
            ?>
            <div class="col-md-3 col-sm-6 my-3 my-md-0">
                <form action="index.php" method="post">
                    <div class="card shadow">
                        <div>
                            <img src="<?php echo $row['imagine']; ?>" alt="Image1" class="img-fluid card-img-top">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $row['nume']; ?></h5>
                            <h6>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </h6>
                            <p class="card-text">
                            
                            </p>
                            <h5>
                                <small><s class="text-secondary">$<?php echo $row['pret'] + 10; ?></s></small>
                                <span class="price">$<?php echo $row['pret']; ?></span>
                            </h5>
                            <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                            <button type="submit" name="add_to_cart" class="btn btn-warning my-3">Add to Cart</button>
                        </div>
                    </div>
                </form>
            </div>
            <?php
                }
            ?>
        </div>
    </div>

    <footer class="site-footer">
        <div class="footer-container">
            <div class="footer-section">
                <h3>Contact</h3>
                <p>Adresa: Str. Soarelui, Nr. 123, Timișoara</p>
                <p>Telefon: +40 123 456 789</p>
                <p>Email: contact@retrofashion.com</p>
            </div>
            <div class="footer-section">
                <h3>Urmărește-ne</h3>
                <ul class="social-links">
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Instagram</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li><a href="#">Pinterest</a></li>
                </ul>
            </div>
            <div class="footer-section">
                <h3>Despre Noi</h3>
                <p>Retro Fashion oferă cele mai cool haine și accesorii retro pentru stilul tău unic!</p>
            </div>
        </div>
        <div class="copyright">
            <p>&copy; 2024 Retro Fashion. Toate drepturile rezervate.</p>
        </div>
    </footer>

</body>
</html>
